package tech.renamed.piupiu.auth;

public interface AuthServiceInterface {

    byte[] generateSalt();
    byte[] generatePasswordHash(String password, byte[] salt);
    boolean comparePasswordHashes(String password, byte[] hash, byte[] salt);
}
