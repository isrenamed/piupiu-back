package tech.renamed.piupiu.auth;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements AuthServiceInterface {

    @Autowired
    Environment env;

    public byte[] generateSalt() {
        Integer saltSize = env.getProperty("auth.salt.size", Integer.class);
        if (saltSize == null) {
            throw new IllegalStateException();
        }

        byte[] salt = new byte[saltSize];
        SecureRandom random = new SecureRandom();
        random.nextBytes(salt);
        return salt;
    }

    public byte[] generatePasswordHash(String password, byte[] salt) {
        try {
            var algorithmName = env.getProperty("auth.algorithm.name");            
            Mac hmac = Mac.getInstance(algorithmName);
            SecretKeySpec keySpec = new SecretKeySpec(salt, algorithmName);
            hmac.init(keySpec);
            return hmac.doFinal(password.getBytes());
        } catch (NoSuchAlgorithmException | IllegalStateException | java.security.InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean comparePasswordHashes(String password, byte[] hash, byte[] salt) {
        byte[] genHash = generatePasswordHash(password, salt);
        return Arrays.equals(hash, genHash);
    }
}
