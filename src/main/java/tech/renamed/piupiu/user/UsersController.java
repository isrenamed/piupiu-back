package tech.renamed.piupiu.user;

import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.renamed.piupiu.auth.AuthServiceInterface;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    AuthServiceInterface authService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<UserResponse> createUser(@RequestBody UserRequest request) {
        try {

            if (usersRepository.findByUsernameOrEmail(request.getUsername(), request.getEmail()).isPresent()) {
                // return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username or email not valid");
            }

            var salt = authService.generateSalt();
            var hash = authService.generatePasswordHash(request.getPassword(), salt);

            var newUser = modelMapper.map(request, User.class);
            newUser.setSalt(salt);
            newUser.setHash(hash);

            newUser = usersRepository.save(newUser);

            var response = modelMapper.map(newUser, UserResponse.class);

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<UserResponse> loginUser(@RequestBody LoginRequest request) {
        try {

            Optional<User> user = usersRepository.findByUsernameOrEmail(request.getUsernameEmail(), request.getUsernameEmail());
            if (!user.isPresent()) {                
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            if (!authService.comparePasswordHashes(request.getPassword(), user.get().getHash(), user.get().getSalt())) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            var response = modelMapper.map(user, UserResponse.class);
            return ResponseEntity.status(HttpStatus.OK)
                .body(response);
                

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<UserResponse> getUserById(@PathVariable UUID id) {
        Optional<User> user = usersRepository.findById(id);

		if (user.isPresent()) {
            var response = modelMapper.map(user.get(), UserResponse.class);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

    }
}
