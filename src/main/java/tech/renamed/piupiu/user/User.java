package tech.renamed.piupiu.user;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;

@Entity
@Table(name = "users",
    indexes = {
        @Index(columnList = "username,hash"),
        @Index(columnList = "email,hash")
    }
)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(length = 300)
    private String name;

    @Column(length = 50, unique = true)
    private String username;

    @Column(length = 150, unique = true)
    private String email;
    
    private byte[] salt;
    private byte[] hash;

    
    public User(UUID id, String name, String username, String email, byte[] salt, byte[] hash) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.salt = salt;
        this.hash = hash;
    }

    public User(){}

    public byte[] getSalt() {
        return salt;
    }
    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
    public byte[] getHash() {
        return hash;
    }
    public void setHash(byte[] hash) {
        this.hash = hash;
    }
    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    

}
