package tech.renamed.piupiu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiupiuApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PiupiuApiApplication.class, args);
	}

}
